<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::resource('temperature', 'TemperatureController', ['except' => ['create', 'edit']]);
Route::get('temperature_min', 'TemperatureController@getMin');

Route::resource('systemlog', 'SystemLogController', ['except' => ['create', 'edit']]);
Route::get('config', 'SystemLogController@getConfiguration');
Route::post('config', 'SystemLogController@setConfiguration');

Route::post('connection', 'TemperatureController@mailConnection', ['except' => ['create', 'edit']]);
