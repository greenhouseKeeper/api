<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TemperatureLog extends Model
{
    protected $fillable = ["celsius_temperature"];
    protected $table = "temperaturelog";
}
