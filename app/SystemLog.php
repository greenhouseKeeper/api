<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SystemLog extends Model
{
    protected $fillable = ['tipo', 'evento'];
    protected $table = 'systemlog';
}
