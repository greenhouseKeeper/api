<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\TemperatureLog;
use App\SystemLog;

class TemperatureController extends Controller
{
    public function index()
    {
        $data = TemperatureLog::orderBy('created_at', 'desc')->get();

        return response()->json($data, 200, [], JSON_PRETTY_PRINT);
    }

    public function store(Request $request)
    {
        $data = $request->all();

        $insert = TemperatureLog::create($data);

        $temperature = $request->get('celsius_temperature');
        $min = SystemLog::where('id', '=', '2')->select('evento')->get();
        $max = SystemLog::where('id', '=', '3')->select('evento')->get();
        
        if ($temperature < $min[0]["evento"] || $temperature > $max[0]["evento"]) {
            $this->mailAlert();
        }

        if ($insert) {
            return response()->json([$insert], 201);
        } else {
            return response()->json(['error'=>'error_insert'], 500);
        }
    }

    public function show($id)
    {
        $data = TemperatureLog::find($id);
        if($data) {
            return response()
                ->json($data, 200, [], JSON_PRETTY_PRINT);
        } else {
            return response()
                ->json(['error'=>'not_found'], 404);
        }
    }

    public function update(Request $request, $id)
    {
        $reg = TemperatureLog::find($id);

        if ($reg) {
            $data = $request->all();

            $alt = $reg->update($data);

            if ($alt) {
                return response()
                    ->json($reg, 200, [], JSON_PRETTY_PRINT);
            } else {
                return response()
                    ->json(['error'=>'not_updated'], 500);
            }
        } else {
            return response()
                ->json(['error'=>'not_found'], 404);
        }
    }

    public function destroy($id)
    {
        $reg = TemperatureLog::find($id);

        if ($reg) {
            if ($reg->delete()) {
                return response()
                    ->json(["msg"=>'Deleted'], 200);
            } else {
                return response()
                    ->json(['error'=>'not_destroy'], 500);
            }
        } else {
            return response()
                ->json(['error'=>'not_found'], 404);
        }
    }

    public function getMin ()
    {
        $data = TemperatureLog::orderBy('created_at', 'desc')->limit(12)->get();

        return response()->json($data, 200, [], JSON_PRETTY_PRINT);
    }

    public function mailAlert()
    {
        $data = SystemLog::where('id', '=', '1')->select('evento')->get();
        $email = $data[0]["evento"];

        \Mail::send('mail.alert', ['temperature' => 'temp'], function($m) use ($email) {
            $m->from('greenhousemailsender@gmail.com', 'GreenHouse Keeper');
            $m->subject('Alerta de Temperatura');
            $m->to($email);
        });
    }

    public function mailConnection(Request $request)
    {
        $data = SystemLog::where('id', '=', '1')->select('evento')->get();
        $email = $data[0]["evento"];

        \Mail::send('mail.connectionLost', ['temperature' => 'temp'], function($m) use ($email) {
            $m->from('greenhousemailsender@gmail.com', 'GreenHouse Keeper');
            $m->subject('Falha de Conexão');
            $m->to($email);
        });
    }
    
}
