<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SystemLog;

class SystemLogController extends Controller
{
    public function index()
    {
        $data = SystemLog::orderBy('created_at')->get();

        return response()->json($data, 200, [], JSON_PRETTY_PRINT);
    }

    public function store(Request $request)
    {
        $data = $request->all();

        $insert = SystemLog::create($data);

        if ($insert) {
            return response()->json([$insert], 201);
        } else {
            return response()->json(['error'=>'error_insert'], 500);
        }
    }

    public function show($id)
    {
        $data = SystemLog::find($id);
        if($data) {
            return response()
                ->json($data, 200, [], JSON_PRETTY_PRINT);
        } else {
            return response()
                ->json(['error'=>'not_found'], 404);
        }
    }

    public function update(Request $request, $id)
    {
        $reg = SystemLog::find($id);

        if ($reg) {
            $data = $request->all();

            $alt = $reg->update($data);

            if ($alt) {
                return response()
                    ->json($reg, 200, [], JSON_PRETTY_PRINT);
            } else {
                return response()
                    ->json(['error'=>'not_updated'], 500);
            }
        } else {
            return response()
                ->json(['error'=>'not_found'], 404);
        }
    }

    public function destroy($id)
    {
        $reg = SystemLog::find($id);

        if ($reg) {
            if ($reg->delete()) {
                return response()
                    ->json(["msg"=>'Deleted'], 200);
            } else {
                return response()
                    ->json(['error'=>'not_destroy'], 500);
            }
        } else {
            return response()
                ->json(['error'=>'not_found'], 404);
        }
    }

    // CONFIGURAÇÕES DE USUÁRIO

    public function setConfiguration(Request $request)
    {
        $email = $request->get('email');
        $min = $request->get('min');
        $max = $request->get('max');

        $reg = SystemLog::find(1);
        $data = ['tipo' => 'configuration',
                'evento' => $email];
        $insert = $reg->update($data);

        $reg = SystemLog::find(2);
        $data = ['tipo' => 'configuration',
                'evento' => $min];
        $insert = $reg->update($data);

        $reg = SystemLog::find(3);
        $data = ['tipo' => 'configuration',
                'evento' => $max];
        $insert = $reg->update($data);

        if ($insert) {
            return response()->json([$insert], 201);
        } else {
            return response()->json(['error'=>'error_insert'], 500);
        }
    }

    public function getConfiguration()
    {
        $email = SystemLog::where('id', '=', '1')->select('evento')->get();
        $min = SystemLog::where('id', '=', '2')->select('evento')->get();
        $max = SystemLog::where('id', '=', '3')->select('evento')->get();
        $config = ["email" => $email, "min" => $min, "max" => $max];

        return response()->json($config, 200, [], JSON_PRETTY_PRINT);
    }
}
