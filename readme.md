## Postman

[https://www.getpostman.com/collections/36f233e8fc11d26468f0](https://www.getpostman.com/collections/36f233e8fc11d26468f0)

## Heroku

[http://frozen-bayou-51290.herokuapp.com/api/](https://www.getpostman.com/collections/36f233e8fc11d26468f0)

## Wiki

[https://gitlab.com/greenhouseKeeper/api/wikis/pages](https://gitlab.com/greenhouseKeeper/api/wikis/pages)

# Instalação

## Requisitos:

 *  Composer (https://getcomposer.org/doc/00-intro.md)
 *  Laravel (Após instalar o composer, rodar o comando: composer global require laravel/installer)
 *  Git (https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
 *  MySQL

## Clone do Repositório:

*  Entre no diretório que deseja colocar o projeto
*  No prompt: git clone git@gitlab.com:greenhouseKeeper/api.git
*  Entre no diretório criado: cd api
*  Comando no prompt: composer update
*  Inicializando: php artisan serve
*  No browser de sua preferência: localhost:8000